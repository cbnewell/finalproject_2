<%--
  Created by IntelliJ IDEA.
  User: Brycen Newell
  Date: 12/11/2020
  Time: 11:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8"  %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <style>
        head {
            justify-content: normal;
        }
        body {
            text-align: center;
            display: flex;
            justify-content: center;
            background-color: lightgray;
        }
        label {
            text-align: left;
        }
        input[type=text], select, input[type=password], input[type=date] {
            text-align: left;
            width: 100%;
            padding: 12px 20px;
            margin: 8px 2px;
            border: 1px solid #000000;
            border-radius: 2px;
            box-sizing: border-box;
        }
        input[type=submit] {

            width: 100%;
            background-color: navy;
            color: whitesmoke;
            padding: 12px 20px;
            margin: 8px 2px;
            border: none;
            border-radius: 2px;

        }
        div {
            margin: 0 auto;
            width: 50%;
            border-radius: 2px;
            background-color: whitesmoke;
            padding: 12px 20px;
        }
    </style>

    <title> Library Checkout Form </title>
</head>


<body>


<br />
<h1 style="padding: 20px; background-color: navy; color: white; ">   Please enter information on the item you are adding to the library: </h1>
<br />
<div>
    <form action="itemServlet" method="post">


        <label style="font-weight: bold; text-decoration: underline;"> Item Name </label>
        <br />
        <label>
            <input type="text" name="itemName" placeholder="Please enter Item Name: " required>
        </label>
        <br />

        <label style="font-weight: bold; text-decoration: underline;"> Item Type </label>
        <br />
        <label>
            <input type="text" name="itemType" placeholder="Please enter Item Type: " required>
        </label>
        <br />

        <label style="font-weight: bold; text-decoration: underline;"> Item Avail </label>
        <br />
        <label>
            <input type="text" name="itemAvail" placeholder="Please enter 'yes' or 'no'" required>
        </label>

        <input style="font-weight: bold; text-decoration: underline;" type="submit" value="Add Item">


    </form>
</div>
</body>


</html>