<%--
  Created by IntelliJ IDEA.
  User: Brycen Newell
  Date: 12/13/2020
  Time: 12:19 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8"  %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <style>
        head {
            justify-content: normal;
        }
        body {
            text-align: center;
            display: flex;
            justify-content: center;
            background-color: lightgray;
        }
        label {
            text-align: left;
        }
        input[type=text], select, input[type=password], input[type=date] {
            text-align: left;
            width: 100%;
            padding: 12px 20px;
            margin: 8px 2px;
            border: 1px solid #000000;
            border-radius: 2px;
            box-sizing: border-box;
        }
        input[type=submit] {

            width: 100%;
            background-color: navy;
            color: whitesmoke;
            padding: 12px 20px;
            margin: 8px 2px;
            border: none;
            border-radius: 2px;

        }
        div {
            margin: 0 auto;
            width: 50%;
            border-radius: 2px;
            background-color: whitesmoke;
            padding: 12px 20px;
        }
    </style>

    <title> Library Login Error </title>
</head>


<body>
<div>
    <br />
    <h3 style="color: darkred; background-color: lightgray; padding: 20px;"> Sorry, There Was An Error With Your Output </h3>
    <h3 style="padding: 20px; background-color: navy; color: white; ">   Please re-enter your login information: </h3>
    <br />
</div>
<div>
    <form action="loginServlet" method="post">


        <label style="font-weight: bold; text-decoration: underline;"> Username </label>
        <br />
        <label>
            <input type="text" name="username" placeholder="username... " required>
        </label>
        <br />

        <label style="font-weight: bold; text-decoration: underline;"> Password </label>
        <br />
        <label>
            <input type="text" name="password" placeholder="password... " required>
        </label>
        <br />

        <input style="font-weight: bold; text-decoration: underline;" type="submit" value="Login" onclick="openPage('index.jsp')" />


    </form>
</div>
</body>


</html>
