package ServletFiles;

import HibernateFiles.DAO;
import HibernateFiles.libraryItem;
import ResponseFiles.itemResponse;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/itemServlet", name = "itemServlet")
public class itemServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {


        PrintWriter out = null;

        String itemName = request.getParameter("itemName").toLowerCase();
        String itemType = request.getParameter("itemType").toLowerCase();
        String itemAvail = request.getParameter("itemAvail").toLowerCase();

        libraryItem libraryItem = new libraryItem();
        libraryItem.setItemName(itemName);
        libraryItem.setItemType(itemType);
        libraryItem.setItemAvail(itemAvail);

        String success = "/itemSuccess.jsp";

        try{
            response.setContentType("text/html");

            out = response.getWriter();


            out.println(itemResponse.returnItemResponse(itemName, itemType, itemAvail));
            if (itemResponse.validation(itemName,itemType,itemAvail) == true) {


                RequestDispatcher requestDispatcher = request.getRequestDispatcher(success);
                requestDispatcher.forward(request,response);
            }

        } catch ( Exception e) {
            out.println("There seems to be an error with our system. Please return later.");
        } catch (ExceptionInInitializerError e) {
            out.println("The database would not connect. Please return and we will fix the problem.");
            System.out.println(e.getStackTrace());

            throw e;
        }




    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        this.doPost(request,response);
    }

}
