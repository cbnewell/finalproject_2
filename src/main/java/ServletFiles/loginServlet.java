package ServletFiles;

import HibernateFiles.userLogin;
import ResponseFiles.loginResponse;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/loginServlet", name = "loginServlet")
public class loginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        PrintWriter out = null;

        String username = request.getParameter("username");
        String password = request.getParameter("password");

        userLogin userlogin = new userLogin();
        userlogin.setUsername(username);
        userlogin.setPassword(password);

        String success = "/index.jsp";
        String failed = "/loginError.jsp";

        try{
            response.setContentType("text/html");

            out = response.getWriter();

            out.println(loginResponse.returnLoginResponse(username,password));

// should call new Item Servlet on successful login response and refresh page on a failed response
            if(loginResponse.Authentication(username,password) == true && loginResponse.returnMatch(username,password) == true) {
                RequestDispatcher requestDispatcher = request.getRequestDispatcher(success);
                requestDispatcher.forward(request,response);
            } else {
                RequestDispatcher requestDispatcher = request.getRequestDispatcher(failed);
                requestDispatcher.forward(request,response);
            }

        } catch ( Exception e) {
            out.println("There seems to be an error with our system. Please return later.");
        } catch (ExceptionInInitializerError e) {
            out.println("The database would not connect. Please return and we will fix the problem.");
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        this.doPost(request,response);
    }

}