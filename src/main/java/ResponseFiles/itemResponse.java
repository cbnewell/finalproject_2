package ResponseFiles;

import HibernateFiles.DAO;
import HibernateFiles.libraryItem;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class itemResponse {

    public static String returnItemResponse(String itemName, String itemType, String itemAvail) {

        String responseRecorded = "Your item has been added to the library database.";
        String responseFailed = "Your Item could not be added due to a typo or a rule error.";
        String htmlResponseFailed = "<html><head><title>Library Database Failed</title></head> <body><h1>" + responseFailed + "</h1> </body> </html>";


        if (validation(itemName, itemType, itemAvail) == true) {
            libraryItem newLibraryItem = new libraryItem(itemName, itemType, itemAvail);
            DAO newInstance = DAO.getInstance();

            newInstance.addItem(newLibraryItem);

            int ItemID = newLibraryItem.getItemID();

            libraryItem li  = newInstance.getItemByID(ItemID);

            String ItemString = li.toString();

            String htmlResponseRecorded = "<html><head><title>Library Database Filled</title></head> <body><h1>" + responseRecorded + "</h1><p>" + ItemString +"</p></body></html>";


            return htmlResponseRecorded;
        } else {
            return htmlResponseFailed;
        }

    }

    public static boolean validation(String itemName, String itemType, String itemAvail) {
        int maxNameLength = 45;
        int maxTypeLength = 5;
        int maxAvailLength = 3;

        Pattern itemNamePattern = Pattern.compile("[a-zA-Z0-9#&'?/.\\s ]");
        Pattern itemTypePattern = Pattern.compile("[a-zA-Z\\s ]");
        Pattern itemAvailPattern = Pattern.compile("[a-zA-Z\\s ]");

        if ((itemName != null && itemName.length() <= maxNameLength) && (itemType != null && itemType.length() <= maxTypeLength) && (itemAvail != null && itemAvail.length() <= maxAvailLength)) {

            Matcher nameMatcher = itemNamePattern.matcher(itemName);
            Matcher typeMatcher = itemTypePattern.matcher(itemType);
            Matcher availMatcher = itemAvailPattern.matcher(itemAvail);

            boolean nameMatch = nameMatcher.find();
            boolean typeMatch = typeMatcher.find();
            boolean availMatch = availMatcher.find();

            if (nameMatch || typeMatch || availMatch) {
                return true;

            }

        } else {
            return false;
        }
        return false;

    }


}

