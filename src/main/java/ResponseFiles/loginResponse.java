package ResponseFiles;

import HibernateFiles.DAO;
import HibernateFiles.userLogin;


import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class loginResponse {

    public static String returnLoginResponse(String Username, String Password) {



        String loginResponseRecorded = "You have been successfully logged in";
        String failedResponseRecorded = "Sorry, it appears you may have typed something incorrectly.";
        String failedLoginResponse ="<html><head><title>User Authentication FailedAut</title></head> <body><h1>" + failedResponseRecorded + "</h1></body></html>";
        String htmlLoginResponse = "<html><head><title>User Authentication Success</title></head> <body><h1>" + loginResponseRecorded + "</h1></body></html>";

        if (Authentication(Username, Password)==true) {
            if (returnMatch(Username, Password)==true) {


                // htmlLoginResponse will no longer show its value but it will redirect to the correct page when true
                return htmlLoginResponse;
                // then should direct to proper viewItem Servlet for next step
            }
        } else {
            return failedLoginResponse;
        }
        return failedLoginResponse;

    }

    public static boolean Authentication(String Username, String Password) {
        int maxLength = 30;

        Pattern usernamePattern = Pattern.compile("[a-zA-Z0-9]");
        Pattern passwordPattern = Pattern.compile("[a-zA-Z0-9_#$%&’*+/=?^.-]");

        if((Username != null && Username.length() <= maxLength) && (Password != null && Password.length() <= maxLength)) {

            Matcher userMatcher = usernamePattern.matcher(Username);
            Matcher passMatcher = passwordPattern.matcher(Password);

            boolean userMatch = userMatcher.find();
            boolean passMatch = passMatcher.find();

            if (userMatch || passMatch) {
                return true;

            }
        } else {
            return false;
        }
        return false;
    }



    public static boolean returnMatch(String Username, String Password) {

        DAO newInstance = DAO.getInstance();
        List<userLogin> users = newInstance.getUsers();

        for (userLogin i : users) {
            if ((i.getUsername().equalsIgnoreCase(Username)) && i.getPassword().equalsIgnoreCase(Password)) {
                return true;
            }
        }
        return false;
    }

}
