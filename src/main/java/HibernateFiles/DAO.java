package HibernateFiles;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;


public class DAO {

    SessionFactory sessionFactory = null;
    Session session = null;

    private static DAO single_instance = null;

    public DAO() {
        sessionFactory = DatabaseSession.getFactory();

    }

    public static DAO getInstance() {
        if (single_instance == null) {
            single_instance = new DAO();
        }
        return single_instance;
    }



    public libraryItem getItemByID(int ItemID) {

        session = sessionFactory.openSession();
        session.getTransaction().begin();
        String sql = "from HibernateFiles.libraryItem where ItemID=" + ItemID;

        libraryItem myItem = (libraryItem) session.createQuery(sql).getSingleResult();

        session.getTransaction().commit();
        session.close();
        return myItem;

    }

    public void addItem(libraryItem newItem){

        //test a db object
        session = sessionFactory.openSession();
        session.getTransaction().begin();
        session.save(newItem);
        session.getTransaction().commit();

    }
    public List<libraryItem> getItems() {

        session = sessionFactory.openSession();
        session.getTransaction().begin();

        String sql = "from HibernateFiles.libraryItem";
        List<libraryItem> itemList = (List<libraryItem>) session.createQuery(sql).getResultList();

        session.getTransaction().commit();
        session.close();

        return itemList;


    }

    public List<userLogin> getUsers() {

        session = sessionFactory.openSession();
        session.getTransaction().begin();

        String sql = "from HibernateFiles.userLogin";
        List<userLogin> userLoginList = (List<userLogin>) session.createQuery(sql).getResultList();

        session.getTransaction().commit();
        session.close();

        return userLoginList;


    }





}

