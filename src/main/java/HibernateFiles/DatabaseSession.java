package HibernateFiles;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;


public class DatabaseSession {

    private static final SessionFactory sessionFactory = createFactory();

    private static SessionFactory createFactory() {

        try {
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml" ).build();

            Metadata metadata = new MetadataSources(serviceRegistry).getMetadataBuilder().build();

            return  metadata.getSessionFactoryBuilder().build();
        } catch (Throwable ex) {
            System.err.println("Session Factory could not be created." + ex);
            throw new ExceptionInInitializerError(ex);
        }



    }

    public static SessionFactory getFactory() {
        return sessionFactory;
    }

    public static void shutdown() {
        getFactory().close();
    }
}