package HibernateFiles;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "itemdb")
public class libraryItem implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ItemID")
    public int ItemID;
    @Column(name="ItemName")
    public String ItemName;
    @Column(name = "ItemType")
    public String ItemType;
    @Column(name = "ItemAvail")
    public String ItemAvail;



    public libraryItem() {

    }

    public libraryItem(String ItemName, String ItemType, String ItemAvail) {

        super();
        this.ItemName = ItemName;
        this.ItemType = ItemType;
        this.ItemAvail = ItemAvail;


    }

    public int getItemID() {
        return ItemID;
    }

    public void setItemID(int ItemID) {
        this.ItemID = ItemID;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String ItemName) {
        this.ItemName = ItemName;
    }

    public String getItemType() {
        return ItemType;
    }

    public void setItemType(String ItemType) {
        this.ItemType = ItemType;
    }

    public String getItemAvail(){return  ItemAvail;}

    public void setItemAvail(String ItemAvail) { this.ItemAvail = ItemAvail;}


    public String toString() {


        return ("ITEM NAME: " + ItemName + "<br />" + "ITEM TYPE: " + ItemType + "<br />" + "ITEM AVAILABLE: " + ItemAvail);
    }
}
