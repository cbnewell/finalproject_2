<%--
  Created by IntelliJ IDEA.
  User: Brycen Newell
  Date: 12/11/2020
  Time: 11:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8"  %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <style>
        head {
            justify-content: normal;
        }
        body {
            text-align: center;
            display: flex;
            justify-content: center;
            background-color: lightgray;
        }
        label {
            text-align: left;
        }
        input[type=text], select, input[type=password], input[type=date] {
            text-align: left;
            width: 100%;
            padding: 12px 20px;
            margin: 8px 2px;
            border: 1px solid #000000;
            border-radius: 2px;
            box-sizing: border-box;
        }
        input[type=submit] {

            width: 100%;
            background-color: navy;
            color: whitesmoke;
            padding: 12px 20px;
            margin: 8px 2px;
            border: none;
            border-radius: 2px;

        }
        div {
            margin: 0 auto;
            width: 50%;
            border-radius: 2px;
            background-color: whitesmoke;
            padding: 12px 20px;
        }
    </style>

    <title> Item Added Successfully </title>
</head>


<body>


<br />
<h1 style="padding: 20px; background-color: navy; color: white; "> Your Item Has Been Added Successfully </h1>
<br />
<div>

    <a href="index.jsp"> Add Another Item </a>

</div>
</body>


</html>